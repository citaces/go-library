package models

type User struct {
	ID          int    `bd:"id" json:"id"`
	Name        string `bd:"name" json:"name"`
	RentedBooks []Book `json:"rentedBooks,omitempty"`
}
