package models

type Author struct {
	ID    int    `bd:"id" json:"id"`
	Name  string `bd:"name" json:"name"`
	Books []Book `bd:"books" json:"books,omitempty"`
	Top   *Top   `json:"top,omitempty"`
}

type Top struct {
	Readers int `json:"readers"`
}
