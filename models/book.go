package models

import "database/sql"

type Book struct {
	ID         int            `db:"id" json:"id"`
	Title      string         `db:"title" json:"title"`
	AuthorID   sql.NullInt64  `db:"author_id" json:"-"`
	AuthorName sql.NullString `db:"author_name" json:"-"`
	UserID     sql.NullInt64  `db:"user_id" json:"-"`
	UserName   sql.NullString `db:"user_name" json:"-"`
	User       *User          `json:"user"`
	Author     *Author        `json:"author"`
}
