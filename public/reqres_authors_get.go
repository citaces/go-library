package public

import "gitlab.com/citaces/go-library/models"

// swagger:route GET /authors authors authorsGetListRequest
// List of authors.
// responses:
// 200: authorsGetListResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters authorsGetListRequest
//
//nolint:all

// swagger:response authorsGetListResponse
//
//nolint:all
type authorsGetListResponse struct {
	//in:body
	Body []models.Author
}

// swagger:route GET /authors/top authors topMostReadAuthorsRequest
// Top most read authors.
// responses:
// 200: topMostReadAuthorsResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters topMostReadAuthorsRequest
//
//nolint:all

// swagger:response topMostReadAuthorsResponse
//
//nolint:all
type topMostReadAuthorsResponse struct {
	//in:body
	Body []models.Author
}
