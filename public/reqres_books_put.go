package public

// swagger:route PUT /books/return/{book_id}/users/{user_id} books returnBookRequest
// Put the book back in the library.
// responses:
// 200: returnBookResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters returnBookRequest
//
//nolint:all
type returnBookRequest struct {
	// book ID
	//
	// required: true
	// in: path
	BookID int `json:"book_id"`
	// user ID
	//
	// required: true
	// in: path
	UserID int `json:"user_id"`
}

// swagger:response returnBookResponse
//
//nolint:all
type returnBookResponse struct {
	//in:body
	Status string
}

// swagger:route PUT /books/takes/{book_id}/users/{user_id} books takesBookByUserRequest
// Get book by user.
// responses:
// 200: takesBookByUserResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters takesBookByUserRequest
//
//nolint:all
type takesBookByUserRequest struct {
	// book ID
	//
	// required: true
	// in: path
	BookID int `json:"book_id"`
	// user ID
	//
	// required: true
	// in: path
	UserID int `json:"user_id"`
}

// swagger:response takesBookByUserResponse
//
//nolint:all
type takesBookByUserResponse struct {
	//in:body
	Status string
}
