// go-library
//
//	Schemes:
//	- http
//	- https
//	BasePath: /
//	Version: 1.0.0
//
//
//	Security:
//	- basic
//
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package public

//go:generate swagger generate spec -o ./swagger.json --scan-models
