package public

// swagger:route POST /books/{AuthorId} books bookAddRequest
// Add new book.
// responses:
// 200: bookAddResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters bookAddRequest
//
//nolint:all
type bookAddRequest struct {
	//Author's ID
	//required:true
	//in:path
	AuthorId int
	//Book title
	//
	// required:true
	// in:formData
	Title string `json:"title"`
}

// swagger:response bookAddResponse
//
//nolint:all
type bookAddResponse struct {
	//in:body
	Status string
}
