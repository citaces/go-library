package public

import "gitlab.com/citaces/go-library/models"

// swagger:route GET /books books booksListRequest
// List of books.
// responses:
// 200: booksListResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters booksListRequest
//
//nolint:all

// swagger:response booksListResponse
//
//nolint:all
type booksListResponse struct {
	//in:body
	Body []models.Book
}
