package public

import "gitlab.com/citaces/go-library/models"

// swagger:route GET /users users usersListRequest
// List of users.
// responses:
// 200: usersListResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters usersListRequest
//
//nolint:all

// swagger:response usersListResponse
//
//nolint:all
type usersListResponse struct {
	//in:body
	Body []models.User
}
