package public

// swagger:route POST /authors authors authorAddRequest
// Add new author.
// responses:
// 200: authorAddResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters authorAddRequest
//
//nolint:all
type authorAddRequest struct {
	//Author's name
	//
	// required:true
	// in:formData
	Name string `json:"name"`
}

// swagger:response authorAddResponse
//
//nolint:all
type authorAddResponse struct {
	//in:body
	Status string
}
