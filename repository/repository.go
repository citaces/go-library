package repository

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sort"

	"github.com/jmoiron/sqlx"
	"gitlab.com/citaces/go-library/models"
)

type Repository interface {
	AuthorAdd(ctx context.Context, name string) error
	AuthorsGetList(ctx context.Context) ([]models.Author, error)
	AuthorsTopList(ctx context.Context) ([]models.Author, error)
	BookAdd(ctx context.Context, authorId int, title string) error
	BooksList(ctx context.Context) ([]models.Book, error)
	ReturnBook(ctx context.Context, bookId int, userId int) error
	UsersList(ctx context.Context) ([]models.User, error)
	GetBookByUser(ctx context.Context, bookId int, userId int) error
}

type Repo struct {
	db *sqlx.DB
}

func NewRepository(db *sqlx.DB) *Repo {
	return &Repo{db: db}
}

func (r *Repo) AuthorAdd(ctx context.Context, name string) error {
	_, err := r.db.ExecContext(ctx, "INSERT INTO authors (name) VALUES ($1)", name)
	if err != nil {
		return errors.New("an unknown error occurred while creating the author")
	}
	return nil
}

func (r *Repo) AuthorsGetList(ctx context.Context) ([]models.Author, error) {
	var authors []models.Author
	err := r.db.SelectContext(ctx, &authors, "SELECT * FROM authors")
	if err != nil {
		log.Fatal(err)
	}
	for i := range authors {
		var books []models.Book
		err := r.db.SelectContext(ctx, &books, `
	SELECT b.id, b.title, b.author_id, a.name as author_name, b.user_id, u.name as user_name
	FROM books b
	LEFT JOIN authors a ON b.author_id = a.id
	LEFT JOIN users u ON b.user_id = u.id
	WHERE b.author_id =$1
`, authors[i].ID)
		if err != nil {
			log.Fatal(err)
		}

		for z := range books {
			if books[z].UserID.Valid {
				u := models.User{
					ID:   int(books[z].UserID.Int64),
					Name: books[z].UserName.String,
				}
				books[z].User = &u
			}
			if books[z].AuthorID.Valid {
				a := models.Author{
					ID:   int(books[z].AuthorID.Int64),
					Name: books[z].AuthorName.String,
				}
				books[z].Author = &a
			}
		}
		authors[i].Books = books
	}
	return authors, nil
}

func (r *Repo) AuthorsTopList(ctx context.Context) ([]models.Author, error) {
	var books []models.Book
	var authors []models.Author
	m := make(map[int]int)

	err := r.db.SelectContext(ctx, &books, "SELECT * FROM books WHERE user_id IS NOT NULL")
	if err != nil {
		log.Fatal(err)
	}
	for _, book := range books {
		m[int(book.AuthorID.Int64)]++
	}
	err = r.db.SelectContext(ctx, &authors, "SELECT * FROM authors")
	if err != nil {
		log.Fatal(err)
	}

	for i, author := range authors {
		if val, ok := m[author.ID]; ok {
			var t models.Top
			t.Readers = val
			authors[i].Top = &t
		}
	}

	sort.Slice(authors, func(i, j int) bool {
		readersI := 0
		if authors[i].Top != nil {
			readersI = authors[i].Top.Readers
		}
		readersJ := 0
		if authors[j].Top != nil {
			readersJ = authors[j].Top.Readers
		}
		return readersI > readersJ
	})

	return authors, nil
}

func (r *Repo) BookAdd(ctx context.Context, authorId int, title string) error {
	var count int
	err := r.db.GetContext(ctx, &count, "SELECT count(*) FROM authors WHERE id=$1", authorId)
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("author with ID %d does not exist", authorId)
	}

	_, err = r.db.ExecContext(ctx, "INSERT INTO books (author_id, title) VALUES ($1, $2)", int64(authorId), title)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repo) BooksList(ctx context.Context) ([]models.Book, error) {
	var books []models.Book
	err := r.db.SelectContext(ctx, &books, `SELECT b.id, b.title, b.author_id,a.name as author_name,b.user_id,u.name as user_name 
FROM books b
LEFT JOIN authors a ON b.author_id=a.id
LEFT JOIN users u ON b.user_id=u.id
`)
	if err != nil {
		log.Fatal(err)
	}

	for z := range books {
		if books[z].UserID.Valid {
			u := models.User{
				ID:   int(books[z].UserID.Int64),
				Name: books[z].UserName.String,
			}
			books[z].User = &u
		}
		if books[z].AuthorID.Valid {
			a := models.Author{
				ID:   int(books[z].AuthorID.Int64),
				Name: books[z].AuthorName.String,
			}
			books[z].Author = &a
		}
	}

	return books, nil
}

func (r *Repo) ReturnBook(ctx context.Context, bookId int, userId int) error {
	eff, err := r.db.ExecContext(ctx, "UPDATE books SET user_id=NULL WHERE id=$1 AND user_id=$2", bookId, userId)
	if err != nil {
		log.Fatal(err)
	}
	res, _ := eff.RowsAffected()
	if res == 0 {
		return errors.New("nothing to return")
	}
	return nil
}

func (r *Repo) UsersList(ctx context.Context) ([]models.User, error) {
	var users []models.User
	err := r.db.SelectContext(ctx, &users, `SELECT * FROM users`)
	if err != nil {
		log.Fatal(err)
	}

	for i := range users {
		var books []models.Book
		err := r.db.SelectContext(ctx, &books, `SELECT b.id, b.title, b.author_id, a.name as author_name
	FROM books b
	LEFT JOIN authors a ON b.author_id=a.id
	WHERE user_id=$1`, users[i].ID)

		if err != nil {
			log.Fatal(err)
		}

		for z := range books {
			u := &models.User{
				ID:   users[i].ID,
				Name: users[i].Name,
			}
			books[z].User = u
			if books[z].AuthorID.Valid {
				a := models.Author{
					ID:   int(books[z].AuthorID.Int64),
					Name: books[z].AuthorName.String,
				}
				books[z].Author = &a
			}
		}

		users[i].RentedBooks = books
	}

	return users, nil
}

func (r *Repo) GetBookByUser(ctx context.Context, bookId int, userId int) error {
	var count int
	err := r.db.GetContext(ctx, &count, "SELECT count(*) FROM books WHERE id=$1 AND user_id IS NULL", bookId)
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("book with ID %d is already rented or not ready yet", bookId)
	}

	_, err = r.db.ExecContext(ctx, "UPDATE books SET user_id=$1 WHERE id=$2", userId, bookId)
	if err != nil {
		return err
	}

	return nil
}
