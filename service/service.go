package service

import (
	"context"

	"gitlab.com/citaces/go-library/models"
	"gitlab.com/citaces/go-library/repository"
)

type Service interface {
	AuthorsGetList(ctx context.Context) ([]models.Author, error)
	AuthorsTopList(ctx context.Context) ([]models.Author, error)
	AuthorAdd(ctx context.Context, name string) error
	GetBookByUser(ctx context.Context, bookId int, userId int) error
	BooksList(ctx context.Context) ([]models.Book, error)
	BookAdd(ctx context.Context, authorId int, title string) error
	ReturnBook(ctx context.Context, bookId, userId int) error
	UsersList(ctx context.Context) ([]models.User, error)
}

type Serv struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *Serv {
	return &Serv{repository}
}

func (s *Serv) AuthorsGetList(ctx context.Context) ([]models.Author, error) {
	return s.repository.AuthorsGetList(ctx)
}

func (s *Serv) AuthorsTopList(ctx context.Context) ([]models.Author, error) {
	return s.repository.AuthorsTopList(ctx)
}

func (s *Serv) AuthorAdd(ctx context.Context, name string) error {
	return s.repository.AuthorAdd(ctx, name)
}

func (s *Serv) GetBookByUser(ctx context.Context, bookId int, userId int) error {
	return s.repository.GetBookByUser(ctx, bookId, userId)
}

func (s *Serv) BooksList(ctx context.Context) ([]models.Book, error) {
	return s.repository.BooksList(ctx)
}

func (s *Serv) BookAdd(ctx context.Context, authorId int, title string) error {
	return s.repository.BookAdd(ctx, authorId, title)
}

func (s *Serv) ReturnBook(ctx context.Context, bookId, userId int) error {
	return s.repository.ReturnBook(ctx, bookId, userId)
}

func (s *Serv) UsersList(ctx context.Context) ([]models.User, error) {
	return s.repository.UsersList(ctx)
}
