package main

import (
	"log"

	"gitlab.com/citaces/go-library/handlers"

	"gitlab.com/citaces/go-library/controllers"
	"gitlab.com/citaces/go-library/data"
	"gitlab.com/citaces/go-library/repository"
	"gitlab.com/citaces/go-library/service"
)

func main() {
	db, err := data.NewDB()
	if err != nil {
		log.Fatal(err)
	}
	r := repository.NewRepository(db)
	s := service.NewService(r)
	c := controllers.NewController(s)
	h := handlers.NewHandlers(c)
	h.Route()
}
