package handlers

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/citaces/go-library/controllers"

	"gitlab.com/citaces/go-library/swaggerui"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type Handlers struct {
	controller controllers.Controllers
}

func NewHandlers(controller controllers.Controllers) *Handlers {
	return &Handlers{controller: controller}
}

func (h *Handlers) Route() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/authors", h.controller.AuthorsGetListRequest)
	r.Get("/authors/top", h.controller.AuthorsTopListRequest)
	r.Post("/authors", h.controller.AuthorAddRequest)
	r.Put("/books/takes/{book_id}/users/{user_id}", h.controller.GetBookByUserRequest)
	r.Get("/books", h.controller.BooksListRequest)
	r.Post("/books/{AuthorId}", h.controller.BookAddRequest)
	r.Put("/books/return/{book_id}/users/{user_id}", h.controller.ReturnBookRequest)
	r.Get("/users", h.controller.UsersListRequest)
	r.Get("/", swaggerui.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/",
			http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})
	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}
	go func() {
		log.Printf("server started on port %s", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
