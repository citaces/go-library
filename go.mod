module gitlab.com/citaces/go-library

go 1.19

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/icrowley/fake v0.0.0-20221112152111-d7b7e2276db2
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
)

require (
	github.com/corpix/uarand v0.0.0-20170723150923-031be390f409 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
)
